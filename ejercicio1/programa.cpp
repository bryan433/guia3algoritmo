#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;



int main (void){
	Lista *lista = new Lista();
	int numero;
	char opcion = 's';
	
	while(opcion == 's'){
		
		cout << "inserte un numero entero a su lista enlazada: " << endl;
		cin >> numero;
		
		/* creamos nuesto nodo */ 
		lista->crear(numero);
		lista->Ordenar_lista();
		
		cout << "¿Desea seguir igresando numeros? s(si) / n(no)" << endl;
		cin >> opcion;
		system("clear");
	}
	/* Ordenamos nuestra lista y luego la imprimimos */
	cout << "Mostrando lista\n";
	lista->imprimir();
}
