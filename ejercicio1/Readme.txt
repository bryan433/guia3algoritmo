-Empezando el programa: al inicializar nos pedira ingresar un valor, despues del ingresar el valor nos preguntara si queremos seguir ingresando mas valores o no, mientras estamos ingresando valores estaremos ordenando nuestra lista enlazada para poder imprimirla de mayor a menor, cuando queramos dejar de ingresar valores deberemos colocar una "n" cuando nos pregunte si queremos seguir ingresando valores, al hacer esto, el programa nos imprimimira automaticamente nuestra lista enlazada ordenanda.

-Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/guia3algoritmo clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

-Sistema operativo:
Debian GNU/Linux 10 (buster)

-Autor:
Bryan Ahumada Ibarra
