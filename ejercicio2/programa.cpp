#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;


int main (void){
	Lista *lista = new Lista();
	Lista *lista2 = new Lista();
	Lista *lista3 = new Lista();
	int numero;
	char opcion = 's';
	
	while(opcion == 's'){
		
		cout << "inserte un numero entero a su lista enlazada 1: " << endl;
		cin >> numero;
		
		/* creamos nuestro nodo */ 
		lista->crear(numero);
		lista->Ordenar_lista();
		
		lista3->crear(numero);
		lista3->Ordenar_lista();
			
		cout << "inserte un numero entero a su lista enlazada 2: " << endl;
		cin >> numero;
		
		lista2->crear(numero);
		lista2->Ordenar_lista();
		
		lista3->crear(numero);
		lista3->Ordenar_lista();
		
		cout << "¿Desea seguir igresando numeros? s(si) / n(no)" << endl;
		cin >> opcion;
		system("clear");
	}
	/* Ordenamos nuestra lista y luego la imprimimos */
	cout << "Mostrando lista 1\n";
	lista->imprimir();
	
	cout << "\nMostrando lista 2\n";
	lista2->imprimir();
	
	cout << "\nMostrando lista 3\n";
	lista3->imprimir();
}
