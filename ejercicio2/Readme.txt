-Empezando el programa: al inicializar el programa nos pedira lo mismo que antes, solo que esta ves nos lo pedira dos veces, ya que creamos dos listas en ves de una, al ir ingresando valores, a estas dos listas, estos valores se los estamos pasando a una tercera lista, la cual esta conformada por las otras dos listas, igual que antes, cada ves que ingresamos los dos valores, nos preguntara si queremos seguir o no ingresando valores, al colocar que no, nos imprimira nuestras 3 listas.

-Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/guia3algoritmo clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

-Sistema operativo:
Debian GNU/Linux 10 (buster)

-Autor:
Bryan Ahumada Ibarra
