-Empezando el programa: al inicializar el programa nos pedira ingresar un numero el cual se añadira a nuestra lista, luego de esto nos preguntara si esque queremos seguir insertando numeros a nuestra lista,
cuando terminemos de ingresar los numeros, el programa nos ordenara los numeros ingresados y luego nos completara automaticamente la lista desde el menor hasta el mayor, consecutivamente con los numero que no estan.

-Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/guia3algoritmo clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

-Sistema operativo:
Debian GNU/Linux 10 (buster)

-Autor:
Bryan Ahumada Ibarra
